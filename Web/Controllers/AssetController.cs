﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Assets.Implementation.Commands;
using Core.Assets.Implementation.Commands.Assets;
using Core.Assets.Implementation.Commands.Edges;
using Core.Assets.Implementation.Commands.Risks;
using Core.Assets.Implementation.Commands.Treatments;
using Core.Assets.Implementation.Commands.Vulnerabilities;
using Core.Assets.Interfaces.Services;
using Core.Assets.Models;
using Core.AuditTrail.Implementation.Commands;
using Core.AuditTrail.Interfaces.Services;
using Core.AuditTrail.Models;
using Core.Database.Enums;
using Core.Database.Models;
using Core.Database.Tables;
using Core.Relationships.Implementation.Commands;
using Core.Relationships.Interfaces.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Web.Controllers
{
    [Route("api/assets"), ApiController, EnableCors("CorsRules")]
    public class AssetsController : ControllerBase
    {
        private IAssetService _assetService;
        private IVulnerabilityService _vulnerabilityService;
        private IRelationshipService _relationshipService;
        private ITreatmentService _treatmentService;
        private IRiskService _riskService;
        private IAuditTrailService _auditTrailService;

        public AssetsController(IAssetService assetService, IVulnerabilityService vulnerabilityService, IRelationshipService relationshipService, ITreatmentService treatmentService, IRiskService riskService, IAuditTrailService auditTrailService)
        {
            _assetService = assetService;
            _vulnerabilityService = vulnerabilityService;
            _relationshipService = relationshipService;
            _treatmentService = treatmentService;
            _riskService = riskService;
            _auditTrailService = auditTrailService;
        }

        [HttpPost, ProducesResponseType(201)]
        public async Task<IActionResult> Create([FromBody] CreateAssetCommand command)
        {
            var newValue = await _assetService.Create(command);
            _auditTrailService.LogAction(AuditTrailAction.CreateAsset, newValue.Id, new AuditTrailPayloadModel(){ Data = JsonConvert.SerializeObject(command) });
            return Created(newValue.Id.ToString(), newValue);
        }

        [HttpPut("position"), ProducesResponseType(200)]
        public async Task<IActionResult> MovePosition([FromBody] UpdateAssetPositionCommand command)
        {
            var newValue = await _assetService.MovePosition(command);
            _auditTrailService.LogAction(AuditTrailAction.MoveAsset, command.AssetId, new AuditTrailPayloadModel() { Data = JsonConvert.SerializeObject(command) });
            return Ok(newValue);
        }

        [HttpPost("edge"), ProducesResponseType(201)]
        public async Task<IActionResult> CreateEdge([FromBody] CreateEdgeCommand command)
        {
            var newValue = await _relationshipService.Create(new CreateRelationshipCommand() { FromType = ObjectType.Asset, FromId  = command.Asset1Guid, ToType = ObjectType.Asset, ToId = command.Asset2Guid,
                Payload = JsonConvert.SerializeObject(new AssetEdgePayloadModel(){ Name = command.Name, Asset1Anchor = command.Asset1Anchor, Asset2Anchor = command.Asset2Anchor}) });
            _auditTrailService.LogAction(AuditTrailAction.CreateAssetEdge, newValue.Id, new AuditTrailPayloadModel(){ Data = JsonConvert.SerializeObject(command) });
            return Created(newValue.Id.ToString(), newValue);
        }

        [HttpPost("groups"), ProducesResponseType(201)]
        public async Task<IActionResult> CreateGroup([FromBody] CreateAssetCommand command)
        {
            command.IsGroup = true;
            var newValue = await _assetService.Create(command);
            foreach (var item in command.Assets)
                _relationshipService.Create(new CreateRelationshipCommand() { FromType = ObjectType.AssetGroup, FromId = newValue.Id, ToType = ObjectType.Asset, ToId = item });

            _auditTrailService.LogAction(AuditTrailAction.CreateAssetGroup, newValue.Id, new AuditTrailPayloadModel() { Data = JsonConvert.SerializeObject(command) });
            return Created(newValue.Id.ToString(), newValue);
        }
    }
}